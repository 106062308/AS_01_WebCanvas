# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here

點擊每一個圖案會分別呼叫不同的function 點擊筆刷跟橡皮擦會出現不同游標 畫圓畫方畫三角形如果在換游標我覺得圖沒辦法好好畫 所以改成十字型 能夠更好瞄準
按照順序分別為儲存檔案為png檔、將整個畫面reset，使我們無法返回上一步、 
上一步以及下一步、普通的筆刷、橡皮擦、輸入文字、分別畫出三角形長方形以及圓形的圖案
另外，我還做出可以選擇筆刷的粗細以及顏色、但在選擇字體及大小的部分尚未完成，不過按F12可以發現只要選擇不同的大小以及字體
會console出正確的格式，所以我目前無法得知為何無法成功改變字體大小及字型。

一進入function，首先我會先改變所有flag的狀態以及初始化線條的顏色粗細。

接著，每個function都是呼叫：
canvas.addEventListener('mousedown', function(evt) {
    var mousePos = getMousePos(canvas, evt);
    ctx.beginPath();//準備開始畫 後面接moveto
    ctx.moveTo(mousePos.x, mousePos.y);//從哪開始
    evt.preventDefault();
    canvas.addEventListener('mousemove', mouseMove, false);
  })
這樣的一個Listener，我再在function mouseMove裡面根據tool值的不同來去表現出不同的功能。
  

