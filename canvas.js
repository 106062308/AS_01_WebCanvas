var canvas = document.getElementById('art');
var ctx = canvas.getContext('2d');
var cPushArray = new Array();
var cStep = -1;
var change_color = true;
var tool = 'pen';
var hasInput = false;
var texting = false;

ctx.fillStyle = 'white';
ctx.fillRect(0,0,canvas.width,canvas.height);
cPush();
var pic = new Image();
var startPos;
document.getElementById('undo').addEventListener('click', cUndo, false);
document.getElementById('redo').addEventListener('click', cRedo, false);

if(tool==='pen')ctx.lineWidth=3; 
      else ctx.lineWidth=10;

function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}


function mouseMove(evt) {
    if(tool==='pen'|| tool==='eraser'){
      var mousePos = getMousePos(canvas, evt);
      ctx.lineTo(mousePos.x, mousePos.y);//畫到哪
      ctx.stroke();//顯現
    }
    else if(tool==='circle'){

      ctx.beginPath();
      ctx.drawImage(pic, 0, 0);
      var pos=getMousePos(canvas,evt);
      //console.log(startPos);
      ctx.arc(startPos.x,startPos.y,distance(pos.x,pos.y),0,2*Math.PI);
      ctx.fillStyle=ctx.strokeStyle;
      ctx.stroke();
      ctx.closePath();
    }
    else if(tool==='rect'){
      ctx.beginPath();
      ctx.drawImage(pic, 0, 0);
      var pos=getMousePos(canvas,evt);
      ctx.rect(startPos.x,startPos.y,pos.x-startPos.x, pos.y-startPos.y);
      ///ctx.fillStyle=ctx.strokeStyle;
      ctx.stroke();
      //ctx.fill();//空心實心
      ctx.closePath();
    }

    else if(tool==='tri'){
      ctx.beginPath();
      ctx.drawImage(pic, 0, 0);
      var pos=getMousePos(canvas,evt);
      ctx.moveTo(startPos.x, startPos.y);
      ctx.lineTo(startPos.x-pos.x+startPos.x, startPos.y+pos.y-startPos.y);
      ctx.lineTo(startPos.x+pos.x-startPos.x, startPos.y+pos.y-startPos.y);
      ctx.fillStyle=ctx.strokeStyle;
      ctx.closePath();
      //ctx.fill();
      ctx.stroke();
    }
    
  }
  
  canvas.addEventListener('mousedown', function(evt) {
    var mousePos = getMousePos(canvas, evt);
    ctx.beginPath();//準備開始畫 後面接moveto
    ctx.moveTo(mousePos.x, mousePos.y);//從哪開始
    evt.preventDefault();
    canvas.addEventListener('mousemove', mouseMove, false);
  });

canvas.addEventListener('mousemove', cursor, false);

function cursor(){
    if(tool==='pen'){
      canvas.style.cursor = "url(1.png) ,auto";
    }
    else if(tool==='eraser'){
      canvas.style.cursor = "url(2.png) ,auto";
    }
    else {
      canvas.style.cursor = "crosshair";
    }
  
}

  canvas.addEventListener('mouseup', function() {
    canvas.removeEventListener('mousemove', mouseMove, false);
    //ctx.closePath();
    cPush();
  }, false);

document.getElementById('reset').addEventListener('click', function() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.fillStyle="white";
  ctx.fillRect(0,0,canvas.width,canvas.height);
  cStep = -1;
  //cPushArray.length=0;
  cPush();
}, false);



var colors = ['red', 'blue', 'green', 'purple', 'yellow', 'orange', 'pink', 'black', 'white'];
var size = [3, 5, 10, 15, 20];
var sizeNames = ['three', 'five', 'ten', 'fifteen', 'twenty'];



function listener(i) {
  document.getElementById(colors[i]).addEventListener('click', function() {
    if(change_color == true){
       ctx.strokeStyle = colors[i];
    }
  }, false);
}

function fontSizes(i) {
  document.getElementById(sizeNames[i]).addEventListener('click', function() {
    ctx.lineWidth=size[i];
    console.log(size[i]);
    console.log(size[i]);
  }, false);
}

for(var i = 0; i < colors.length; i++) {
  listener(i);
}

for(var i = 0; i < size.length; i++) {
  fontSizes(i);
}
/////////////////////////////
function cPush() {
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(canvas.toDataURL());
} 

function cUndo() {
    if (cStep > 0) {
        cStep--;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
}

function cRedo() {
    if (cStep < cPushArray.length-1) {
        cStep++;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
}

function savepng(){
    var link = document.createElement('a');
        link.href = canvas.toDataURL();
        link.download = "Untitled";

        link.click();
  }
  /////////////////////

function pen(){
  ///canvas.style.cursor = "https://www.sanrio.com.tw/wp-content/uploads/2018/09/22.July8-01.png";//url(1.png),crosshair
  ctx.strokeStyle = 'black'; 
  ctx.lineWidth=3; 
  change_color = true;
  texting = false;
  tool = 'pen';
  canvas.addEventListener('mousedown', function(evt) {
    var mousePos = getMousePos(canvas, evt);
    ctx.beginPath();//準備開始畫 後面接moveto
    ctx.moveTo(mousePos.x, mousePos.y);//從哪開始
    evt.preventDefault();
    canvas.addEventListener('mousemove', mouseMove, false);
  })
}

function erase(){
  ///canvas.style.cursor = "url(eraser.png),crosshair";
  ctx.strokeStyle = 'white'; 
  ctx.lineWidth=10;  
  texting = false;
  change_color = false;
  tool = 'eraser';
  canvas.addEventListener('mousedown', function(evt) {
    var mousePos = getMousePos(canvas, evt);
    ctx.beginPath();//準備開始畫 後面接moveto
    ctx.moveTo(mousePos.x, mousePos.y);//從哪開始
    evt.preventDefault();
    canvas.addEventListener('mousemove', mouseMove, false);
  })
}



function circle(){
  tool = 'circle';
  texting = false;
  ctx.strokeStyle = 'black';
  change_color = true;
  canvas.addEventListener('mousedown', function(evt) {
     startPos = getMousePos(canvas, evt);
     pic.src = canvas.toDataURL();
     canvas.addEventListener('mousemove', mouseMove, false);
  }) 
}

function rect(){
  tool = 'rect';
  ctx.strokeStyle = 'black';
  texting = false;
  change_color = true;
  canvas.addEventListener('mousedown', function(evt) {
    startPos = getMousePos(canvas, evt);
    pic.src = canvas.toDataURL();
    canvas.addEventListener('mousemove', mouseMove, false);
 }) 
}

function tri(){
  tool = 'tri';
  ctx.strokeStyle = 'black';
  texting = false;
  change_color = true;
  canvas.addEventListener('mousedown', function(evt) {
    startPos = getMousePos(canvas, evt);
    pic.src = canvas.toDataURL();
    canvas.addEventListener('mousemove', mouseMove, false);
 }) 
}

function distance(a,b){
  var first=Math.pow(a-startPos.x,2);
  var second=Math.pow(b-startPos.y,2);
  var third=Math.sqrt(first+second);
  return third;
}

function text() {
  tool = 'text';
  texting = true;
  var text_x, text_y;

  console.log(texting);
  canvas.onclick = function (e) {
      if (hasInput) return;
      if(texting){
      text_x = e.offsetX;
      text_y = e.offsetY;
      addInput(e.clientX, e.clientY);
      }
  }
  function addInput(a, b) {
      var input = document.createElement('input');
      input.type = 'text';
      input.style.position = 'fixed';
      input.style.left = (a - 4) + 'px';
      input.style.top = (b - 4) + 'px';
      input.onkeydown = handleEnter;
      document.body.appendChild(input);
      input.focus();
      hasInput = true;
  }
  function handleEnter(e) {
      var keyCode = e.keyCode;
      if (keyCode === 13) {
          ctx.fillStyle='#000000';
          ctx.font = "30px Arial";
          ctx.fillText(this.value, text_x - 5, text_y + 10);
          document.body.removeChild(this);
          hasInput = false;
          cPush();
      }
  }
  
}

function font_select(){
  ctx.font=document.getElementById("fontsize").value+"px"+" "+document.getElementById("fontselect").value;
  console.log(ctx.font);
}

document.getElementById('file').addEventListener('change', handleImage, false);

function handleImage(evt){
    console.log("ff");
    var reader = new FileReader();
    reader.onload = function(event) {
        var img = new Image();
        img.onload = function() {
            ctx.drawImage(img, 0, 0);
        }
        img.src = event.target.result;
    }
    console.log(evt.target.files);
    reader.readAsDataURL(evt.target.files[0]);
    setTimeout(()=>{cPush()}, 10);
}

